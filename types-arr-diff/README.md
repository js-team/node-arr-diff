# Installation
> `npm install --save @types/arr-diff`

# Summary
This package contains type definitions for arr-diff (https://github.com/jonschlinkert/arr-diff).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/arr-diff

Additional Details
 * Last updated: Mon, 31 Dec 2018 16:16:19 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by BendingBender <https://github.com/BendingBender>.
